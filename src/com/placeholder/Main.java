package com.placeholder;

public class Main {
    public static void main(String[] args) {
        World world = World.fromArgs(args);
        if (world == null) {
            System.out.println("Ungültige Weltparameter");
            return;
        }

        System.out.println(world);

        int rain = world.toggleDownfall();
        System.out.println(world);
        System.out.println(String.format("\nEs wurden %s Blöcke mit Wasser gefüllt", rain));
    }
}
