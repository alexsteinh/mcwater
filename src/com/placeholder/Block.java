package com.placeholder;

public class Block {
    private BlockType type;

    public Block(BlockType type) {
        this.type = type;
    }

    public BlockType getType() {
        return type;
    }

    @Override
    public String toString() {
        switch (type) {
            case GROUND:
                return "X";
            case WATER:
                return "~";
            default:
                return "█";
        }
    }
}
