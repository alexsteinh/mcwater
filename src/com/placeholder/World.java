package com.placeholder;

import java.util.ArrayList;
import java.util.List;

public class World {
    private List<Block>[] chunks;

    private World(int[] data) {
        chunks = new ArrayList[data.length];

        for (int i = 0; i < data.length; i++) {
            int blockCount = data[i];
            List<Block> blocks = new ArrayList<>();
            chunks[i] = blocks;

            for (int j = 0; j < blockCount; j++) {
                blocks.add(new Block(BlockType.GROUND));
            }
        }
    }

    public static World fromArgs(String[] args) {
        int[] data = validateArray(args);
        return data != null ? new World(data) : null;
    }

    private static int[] validateArray(String[] args) {
        int size;
        try {
            size = Integer.parseInt(args[0]);
        } catch (NumberFormatException e) {
            // Keine gültige Zahl
            return null;
        }

        if (args.length != size + 1) {
            // Weniger Parameter als angegeben
            return null;
        }

        int[] data = new int[size];

        for (int i = 0; i < size; i++) {
            try {
                data[i] = Integer.parseInt(args[i + 1]);
            } catch (NumberFormatException e) {
                // Parameter sind keine gültigen Zahlen
                return null;
            }

            if (data[i] < 0) {
                // Keine natürliche Zahl
                return null;
            }
        }

        return data;
    }

    public int toggleDownfall() {
        int waterBlocks = 0;

        for (int i = 0; i < chunks.length; i++) {
            int height = chunks[i].size();
            int heightLeft = i - 1 < 0 ? 0 : chunks[i - 1].size();
            int heightRight = i + 1 > chunks.length - 1 ? 0 : chunks[i + 1].size();

            if (heightLeft > height && heightRight > height) {
                // Hier wird eine Pfütze erstellt
                int rainHeight = Math.abs(height - Math.min(heightLeft, heightRight));
                waterBlocks += rainHeight;

                for (int j = 0; j < rainHeight; j++) {
                    chunks[i].add(new Block(BlockType.WATER));
                }
            }
        }

        return waterBlocks;
    }

    @Override
    public String toString() {
        StringBuilder world = new StringBuilder();
        int height = 0;
        boolean building = true;

        while (building) {
            building = false;

            for (int i = chunks.length - 1; i >= 0; i--) {
                List<Block> blocks = chunks[i];
                if (height < blocks.size()) {
                    world.append(blocks.get(height));
                    world.append(" ");
                    building = true;
                } else {
                    world.append("  ");
                }
            }

            world.append('\n');
            height++;
        }

        return world.reverse().toString();
    }
}
